# CS205 final team 2

For this project we created an endless top-down survival game. The only goal is to survive as long as you can against multiple randomly generated enemies.

Use standard WASD controls to move your character. Move your cursor and press spacebar to fire a projectile in the direction of your cursor. You can pick up different weapons along the way to fight against the endless horde of animals.

The player begins to regenerate health if he is far enough from all animals.

Press 'P' to get out of sticky situations, but you only have two uses!

Press 'N' to stop the music, and press 'M' to stop/restart the music

Press 'esc' to quit the game

## To Run

We recommend running this in Python version 3.11. You may also have to install Python Arcade if you currently do not have it. Once this is done, the program can be run
